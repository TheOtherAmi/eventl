package com.example.user.eventlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.user.eventlist.Model.Event;

/**
 * Created by User on 22/10/2016.
 */

public class EventDetailActivity extends AppCompatActivity{

    private TextView eNameTv,eDescriptionTv,eVenueTv,eDateTv,ePriceTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        eNameTv = (TextView)findViewById(R.id.fragment_event_detail_ename);
        eDescriptionTv = (TextView)findViewById(R.id.fragment_event_detail_edescription);
        eVenueTv = (TextView)findViewById(R.id.fragment_event_detail_evenue);
        eDateTv = (TextView)findViewById(R.id.fragment_event_detail_date);
        ePriceTv = (TextView)findViewById(R.id.fragment_event_detail_price);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Event event = intent.getParcelableExtra("Result");

        eNameTv.setText(event.getEventName());
        eDescriptionTv.setText("Description\n"+event.getEventDescription());
        eVenueTv.setText("Venue : "+event.getEventVenue());
        ePriceTv.setText("Price :RM"+ event.getEventPrice());
        eDateTv.setText("Date   :"+event.getEventDate());

    }


}
