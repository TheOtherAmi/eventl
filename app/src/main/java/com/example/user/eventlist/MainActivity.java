package com.example.user.eventlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.user.eventlist.ui.eventList.EventListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_main_fl,new EventListFragment())
                .commit();
    }
}
