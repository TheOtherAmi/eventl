package com.example.user.eventlist.ui.eventList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.eventlist.CreateEventActivity;
import com.example.user.eventlist.EventDetailFragment;
import com.example.user.eventlist.Model.Event;
import com.example.user.eventlist.R;
import com.example.user.eventlist.EventDetailActivity;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by User on 21/10/2016.
 */

public class EventListFragment extends Fragment implements EventListViewHolder.OnSelectedEventListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int REQUEST_CODE = 021;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FloatingActionButton mFloatingCreateEventBtn;
    private RecyclerView.LayoutManager mLayoutManager;
    private EventListAdapter mEventListAdapter;
    private List<Event> mEventList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_list, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_event_list_swipe_refresh_layout);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_event_list_recyclerviews);
        mFloatingCreateEventBtn = (FloatingActionButton) rootView.findViewById(R.id.fragment_event_list_fab_create_event);

        //set the layout for recyclerview, use linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mEventList = new ArrayList<>();
        mEventListAdapter = new EventListAdapter();

        mEventListAdapter.setEventListener(this);
        mRecyclerView.setAdapter(mEventListAdapter);


        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();
        mFloatingCreateEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CreateEventActivity.class);

                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private List<Event> prepareData() {
        Event event;

        mEventList.removeAll(mEventList);

        event = new Event("Rockaway 2016 - Weekender", "Sepang International Circuit", "29th October 2016 to 30th October 2016");
        event.setEventDescription("Rockaway is the biggest rock festival and the first of its kind in Malaysia. It has in the past featured the likes of Bring Me The Horizon, Dashboard Confessional, You Me At Six, Sum 41, and Paramore among many others. This year, Rockaway will be marked by two major festivals; “Rockaway Weekender” at Sepang International Circuit on October 29 & 30 and the “Rockaway 2016 Finale” on November 19 at Extreme Park.");
        event.setEventPrice(19);
        mEventList.add(event);

        event = new Event("Malaysia Independent Live Fusion Festival (MILFF) 2016", "StarXPO Centre, KWC Fashion Mall, Kuala Lumpur", "22nd October 2016");
        event.setEventDescription("This festival brings to Malaysia a “never-been-done-before” performance of South India’s Top 50 musicians and vocalists. These are independent artists and some are film industry related instrumental performers’ and back ground vocalists. There will be 5 fusion bands performing at the indoor StarXpo Centre, Kuala Lumpur trying to compete and outperform each other. Though this is not a competition, the professional courtesy and competitions amongst the artistes will see a brilliant showdown of musical magic on 22 Oct 2016.");
        event.setEventPrice(20);
        mEventList.add(event);

        event = new Event("EcoWorld MultiGP Drone Racing Championship 2016", "Eco Tropics, Johor Bahru", "1st October 2016 to 2nd October 2016");
        event.setEventDescription("MultiGP Chapters are groups of pilots in a defined region that organize frequent drone racing events to further their skills, competition and progression of our sport.\n" +
                "\n" +
                "MultiGP is an authentic and competitive drone racing organisation for first-person view (FPV) radio-controlled aircraft.  With events and classes for all types of competitors, MultiGP is leading the way in FPV racing and event management. For more information about our events, visit our chapter at: www.multigp.my or our principle MultiGp HQ site at: www.multigp.com");
        event.setEventPrice(20);
        mEventList.add(event);

        event = new Event("Ironman Malaysia", "Langkawi", "12th November 2016");
        event.setEventDescription("Ironman is triathlon’s marquee challenge and the world’s most captivating and testing single day endurance test consisting of a 3.8km swim, a 180km cycle and a 42.2km run.\n" +
                "\n" +
                "In 2012, more than 124,000 athletes from across the globe compete for slots at the prestigious Ironman World Championships held every October in Kailua-Kona, Hawaii. In 2012 only, there are 31 Ironman Triathlon races throughout the world that enable qualification for the Ironman World Championships. The Ironman qualifying events for 2012 include 9 races in Europe, 14 races in North America, 1 race in South America,  5 races in Oceana and 2 races in Africa.");
        event.setEventPrice(21);
        mEventList.add(event);

        event = new Event("Ironman 70.3", "Putrajaya", "3rd April 2016 - Putrajaya");
        event.setEventDescription("Ironman is triathlon’s marquee challenge and the world’s most captivating and testing single day endurance test consisting of a 3.8km swim, a 180km cycle and a 42.2km run.\n" +
                "\n" +
                "In 2012, more than 124,000 athletes from across the globe compete for slots at the prestigious Ironman World Championships held every October in Kailua-Kona, Hawaii. In 2012 only, there are 31 Ironman Triathlon races throughout the world that enable qualification for the Ironman World Championships. The Ironman qualifying events for 2012 include 9 races in Europe, 14 races in North America, 1 race in South America,  5 races in Oceana and 2 races in Africa.");
        event.setEventPrice(100);
        mEventList.add(event);


        mEventListAdapter.notifyDataSetChanged();
        return mEventList;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            Event event = data.getParcelableExtra("NewEvent");


            int position = mEventListAdapter.getEventList().size();

            mEventListAdapter.getEventList().add(event);
            mEventListAdapter.notifyItemInserted(position);
        }


    }


    @Override
    public void onEventSelected(Event event) {
        Intent intent = new Intent(getContext(), EventDetailActivity.class);
        intent.putExtra("Result", event);
        startActivity(intent);

       /* Fragment fragment = new EventDetailFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable("Result",event);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.activity_main_fl, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();*/


    }

    @Override
    public void onClickTextView() {
        Snackbar.make(getView(), "TextBarClicked", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {

        //End Refreshing Animation
        mSwipeRefreshLayout.setRefreshing(false);


        //Load Data

        mEventListAdapter.setEventList(prepareData());
    }
}
