package com.example.user.eventlist.ui.eventList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.eventlist.ui.eventList.EventListViewHolder;
import com.example.user.eventlist.Model.Event;
import com.example.user.eventlist.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 21/10/2016.
 */

public class EventListAdapter extends RecyclerView.Adapter {

    private static final String TAG = "EventListAdapter";
    private List<Event> mEventList = new ArrayList<>();
    private EventListViewHolder.OnSelectedEventListener mEventListener;

    public void setEventListener(EventListViewHolder.OnSelectedEventListener eventListener) {
        mEventListener = eventListener;
    }


    public List<Event> getEventList() {
        return mEventList;
    }

    public void setEventList(List<Event> eventList) {

        mEventList.clear();
        mEventList.addAll(eventList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event_list, parent, false);

        return new EventListViewHolder(view, mEventListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        EventListViewHolder eventListHolder = (EventListViewHolder) holder;
        Event event = mEventList.get(position);

        eventListHolder.setEvent(event);


    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }


}
