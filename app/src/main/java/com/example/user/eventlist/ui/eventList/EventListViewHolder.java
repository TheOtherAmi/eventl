package com.example.user.eventlist.ui.eventList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.user.eventlist.Model.Event;
import com.example.user.eventlist.R;

/**
 * Created by User on 21/10/2016.
 */

public class EventListViewHolder extends RecyclerView.ViewHolder{
    private TextView eventNameTv;
    private TextView eventVenueTv;
    private TextView eventDateTv;



    private Event mEvent;

    public EventListViewHolder(View itemView,final OnSelectedEventListener handler) {
        super(itemView);
        eventNameTv = (TextView)itemView.findViewById(R.id.row_event_list_name);
        eventVenueTv = (TextView)itemView.findViewById(R.id.row_event_list_venue);
        eventDateTv = (TextView)itemView.findViewById(R.id.row_event_list_date);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                handler.onEventSelected(mEvent);
            }
        });
        eventNameTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.onClickTextView();
            }
        });



    }
    public Event getEvent() {
        return mEvent;
    }

    public void setEvent(Event event) {
        mEvent = event;
        updateDisplay();
    }

    private void updateDisplay() {
        eventNameTv.setText(mEvent.getEventName());
        eventVenueTv.setText(mEvent.getEventVenue());
        eventDateTv.setText(mEvent.getEventDate());
    }

    interface OnSelectedEventListener{
        void onEventSelected(Event event);

        void onClickTextView();
    }
}
