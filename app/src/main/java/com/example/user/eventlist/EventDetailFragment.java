package com.example.user.eventlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.eventlist.Model.Event;

/**
 * Created by User on 22/10/2016.
 */

public class EventDetailFragment extends Fragment {
    
    Event event;
    private static final String TAG = "EventDetailFragment";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_event_detail,container,false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            event = bundle.getParcelable("Result");
        }

        Log.d(TAG, "onCreateView: " + event.getEventName());
        return rootView;
    }
}
