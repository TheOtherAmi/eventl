package com.example.user.eventlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.eventlist.Model.Event;

/**
 * Created by User on 21/10/2016.
 */

public class CreateEventActivity extends AppCompatActivity {

    private TextView mEventNameTv;
    private TextView mEventDescriptionTv;
    private TextView mEventDateTv;
    private TextView mEventPriceTv;
    private TextView mEventVenueTv;
    private Button mCreateEventBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        mEventNameTv = (TextView) findViewById(R.id.activity_event_create_event_name);
        mEventDescriptionTv = (TextView) findViewById(R.id.activity_event_create_description);
        mEventDateTv = (TextView) findViewById(R.id.activity_event_create_date);
        mEventPriceTv = (TextView) findViewById(R.id.activity_event_create_price);
        mEventVenueTv = (TextView) findViewById(R.id.activity_event_create_venue);
        mCreateEventBtn = (Button) findViewById(R.id.activity_event_create_date_btn_create_event);

        mCreateEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event event = new Event();
                event.setEventName(mEventNameTv.getText().toString());
                event.setEventDescription(mEventDescriptionTv.getText().toString());
                event.setEventDate(mEventDateTv.getText().toString());
                event.setEventPrice(Integer.parseInt((mEventPriceTv.getText().toString())));
                event.setEventVenue(mEventVenueTv.getText().toString());

                Intent intent = new Intent();
                intent.putExtra("NewEvent",event);

                setResult(RESULT_OK,intent);
                finish();
            }
        });




    }
}
