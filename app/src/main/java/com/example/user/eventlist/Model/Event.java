package com.example.user.eventlist.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 21/10/2016.
 */

public class Event implements Parcelable {

    private String eventName;
    private String eventVenue;
    private String eventDate;
    private String eventDescription;
    private int eventPrice;



    public Event() {
    }

    public Event(String eventName, String eventVenue, String eventDate) {
        this.eventName = eventName;
        this.eventVenue = eventVenue;
        this.eventDate = eventDate;
    }

    protected Event(Parcel in) {
        eventName = in.readString();
        eventVenue = in.readString();
        eventDate = in.readString();
        eventDescription = in.readString();
        eventPrice = in.readInt();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }
    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public int getEventPrice() {
        return eventPrice;
    }

    public void setEventPrice(int eventPrice) {
        this.eventPrice = eventPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventName);
        dest.writeString(eventVenue);
        dest.writeString(eventDate);
        dest.writeString(eventDescription);
        dest.writeInt(eventPrice);
    }
}
